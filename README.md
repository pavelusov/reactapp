# BLUE GREEN DEPLOY settings
## Настройка .gitlab-ci.yml
1. Устанавливаем зависимости
2. Собираем проект и копируем(в докере) сборку для:
   - стэйджинга в папку `/www/testapp/staiging_$CI_COMMIT_SHA`
   - прода в папку `/www/testapp/prod_$CI_COMMIT_SHA`
3. (актвиация на nginx)Делаем симлинки для:
    - стейджинга `/var/www/staiging/$CI_COMMIT_BRANCH` -> `/var/www/testapp/staiging_$CI_COMMIT_HASH`
    - прода `/var/www/html` -> `/var/www/testapp/prod_$CI_COMMIT_HASH`
4. делаем откат на шаг назад
   -  