module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
	"amd": true,
	"node": true
	},
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "overrides": [
	{
		"files": [
			"**/*.spec.js",
			"**/*.test.js",
              "**/*.spec.jsx"
            ],
        "env": {
              "jest": true
            }
        }
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
	"react/jsx-uses-react": "off",
	"react/react-in-jsx-scope": "off"
    }
}
